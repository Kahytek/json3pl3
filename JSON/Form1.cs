﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;

namespace JSON
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            string directory = AppDomain.CurrentDomain.BaseDirectory + "../../Asmenuduomenys/";
            List<Asmuo.PajamosIslaidos> pajamosIslaidos = new List<Asmuo.PajamosIslaidos>();
            Random rnd = new Random();


            Asmuo asmuo = new Asmuo()
            {
                Vardas = "Jonas",
                Pavarde = "Jonaitis",
                GimimoData = new DateTime(1959, 12, 19),
                Atlyginimas = 500.36,
                Ugis = 183,

                Asmuo.Adresas adresas = new Asmuo.Adresas()
                {
                    Miestas = "Vilnius",
                    Gatve = "fabijoniskiu g.",
                    NamoNr = 56,
                }
             };

            

            for (int i = 1; i < 13; i++)
            {
                pajamosIslaidos.Add(new Asmuo.PajamosIslaidos { menesioNr = i, pajamos = rnd.Next(500,1000), islaidos = rnd.Next(0, 2000) });
            }
            

            var json = JsonConvert.SerializeObject(asmuo);

            var json3 = JsonConvert.SerializeObject(pajamosIslaidos);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            var filename = $"{asmuo.Vardas}_{asmuo.Pavarde}_duomenys.json";

            File.WriteAllText(directory + filename, json + pajamosIslaidos);
        }
    }

    public class Asmuo
    {
        public string Vardas { get; set; }
        public string Pavarde { get; set; }
        public DateTime GimimoData { get; set; }
        public double Atlyginimas { get; set; }
        public int Ugis { get; set; }

        public class Adresas
        {
            public string Miestas { get; set; }
            public string Gatve { get; set; }
            public int NamoNr { get; set; }
        }

        public class PajamosIslaidos
        {
            public int menesioNr { get; set; }
            public int pajamos { get; set; }
            public int islaidos { get; set; }
        }
    }
  
}
